class Item < ApplicationRecord
  belongs_to :user

  validates :user, presence: true

  validate :url_or_text

  acts_as_votable
  
  private
    def url_or_text
      unless url.blank? ^ text.blank?
        errors.add(:base, "Specify a URL or text, not both.")
      end
    end

end
